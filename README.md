# url-shortener

This url shortener shortens urls to a short version that users can remember. 

## Installation
1. Clone this repository: `git clone https://gitlab.com/Never_Rei/url-shortener.git`
2. Build web app container: `cd url-shortener && docker-compose build`

### Local Deployment
To run this application locally, you can run `docker-compose up` - however it's recommended that you edit the docker-compose.yaml and set the Redis container password and a secret key.

### Environment Variables
#### url-shortener
- REDIS_HOST
- REDIS_PORT
- REDIS_DB
- REDIS_PASSWORD   
- SECRET_KEY

#### Redis
- ALLOW_EMPTY_PASSWORD
- REDIS_DISABLE_COMMANDS
- REDIS_PASSWORD

## Flow Diagram
![Alt text](/URL_Shortener_Flow_Diagram.png?raw=true "URL Shortener Flow Diagram")