FROM python:3.9-slim

WORKDIR /app
COPY requirements.txt requirements.txt
RUN python3 -m venv venv
RUN /bin/sh venv/bin/activate
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY shortener shortener
RUN pip install -e shortener/.
EXPOSE 8080

ENV REDIS_HOST localhost
ENV REDIS_PORT 6379
ENV REDIS_DB 0

CMD [ "waitress-serve", "--call", "shortener:create_app" ]