from os import getenv
import logging
import base64
import random
from typing import Tuple
from flask.typing import StatusCode

from flask_bootstrap import Bootstrap
from flask import Flask, render_template, redirect, flash, url_for, request, Response
import redis
from markupsafe import escape
from werkzeug.wrappers import response

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
logger = logging.getLogger(__name__)

class Error(Exception):
    """Exception Base Class"""
    pass

class ShorthandExists(Error):
    """Shorthand Already Exists"""
    pass

class ShorthandMissing(Error):
    """Missing shorthand in request"""
    pass

class URLMissing(Error):
    """Missing shorthand in request"""
    pass
    

REDIS_HOST = getenv('REDIS_HOST', "localhost")
REDIS_PORT = getenv('REDIS_PORT', 6379)
REDIS_PASS = getenv('REDIS_PASSWORD', None)
REDIS_DB = getenv("REDIS_DB", 0)

app = Flask(__name__)

class RedisClient():
    def __init__(self, host="localhost", port=6379, db=0, password=None):
        self.logger = logging.getLogger(__name__)
        if password is None or password == "":
            self.logger.warning("Instantiating connection to Redis using passwordless method.")
            self.client = redis.Redis(host=host, port=port, db=0)
        else:
            self.client = redis.Redis(host=host, port=port, db=db, password=password)

redis_client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, password=REDIS_PASS)

def create_app():
    Bootstrap(app)
    if getenv('debug') is True or getenv('debug') == "true":
        logger.setLevel(logging.DEBUG)
    app.config['SECRET_KEY'] = getenv('SECRET_KEY', 'devSECRETkey12345678910')
    return app

@app.route('/')
def home():
    return render_template("index.html")

@app.route('/favicon.ico')
def favicon():
    return Response("No Favicon", status=404)

@app.route('/<shorthand>')
def redirector(shorthand):
    logger.info(f"Recieved redirection request for {shorthand}")
    try:
        url = get_url_from_shorthand(shorthand)
        logger.info(f"Redirecting user to {url}.")
        return redirect(f"{url.decode('utf8')}", code=301)
    except ShorthandMissing:
        flash(f"No shortened url with {shorthand}, cannot redirect.", category="error")
        return redirect(url_for('home'))
    except URLMissing:
        flash(f"Redirect Failed: target url is missing in datastore", category="error")
        return redirect(url_for('home'))

@app.route('/api/shorten', methods=["POST"])
def shorten():
    if request.method == 'POST':    
        url = request.form.get('URL')
        shorthand = request.form.get('shorthand')
        if len(url) == 0 or url is None:
            flash(f"Faled to create shortened URL, target URL is empty.", category="error")
            return redirect(url_for('home'))
        elif len(shorthand) == 0 or shorthand is None:
            flash(f"Faled to create shortened URL, shortened url is empty.", category="error")
            return redirect(url_for('home'))            
        try:
            shortened_url = shorten_url(url, shorthand)
            flash(f"Sucessfully created {request.host_url}{shorthand}", category='message')
            return redirect(url_for('home'))
        except ShorthandExists:
            flash(f"That shortened URL already exists. Please try again.", category="error")
            return redirect(url_for('home'))        
        except:
            flash(f"Faled to create shortened URL", category="error")
            return redirect(url_for('home'))
    else:
        return response('Operation not supported.', StatusCode=400)

def shorten_url(url, shorthand):
    """
    Stores the preferred shortened version of a URL
    """
    if check_url_exists(shorthand):
        logger.error(f"Shorthand: {shorthand}, already exists.")
        raise ShorthandExists
    else:
        try:
            redis_client.client.set(shorthand, url)
            logger.info(f"Stored shortened URL: {shorthand}, target URL: {url}.")
            return shorthand
        except Exception as e:
            logger.error(e)
            raise f"Failed to store shortened URL in redis: {e}"        

def check_url_exists(shorthand):
    """
    This function checks to see if there's a URL already shortened with
    the requested short hand version.
    """
    try:
        return redis_client.client.exists(shorthand)
    except Exception as e:
        logger.error(f"Something went wrong interrogating Redis: {e}")

def get_url_from_shorthand(shorthand):
    """
    This function takes a shorthand url and returns it for use
    within the redirect functionality
    """ 
    if check_url_exists(shorthand):
        pass
    else:
        raise ShorthandMissing

    try:
        url = redis_client.client.get(shorthand)
        logger.debug(f"url is: {url} from shorthand: {shorthand}")
        if url is not None and len(url) > 0:
            logger.debug(f"Found valid shorthand, returning URL.")
            return url
        else:
            raise URLMissing
    except Exception as e:
        logger.error(f"Something went wrong interrogating Redis: {e}")


