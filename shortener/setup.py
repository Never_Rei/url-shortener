from setuptools import setup, find_packages

setup(
    name='shortener',
    author='James Washington',
    author_email='jwashington104.jw@gmail.com',
    url='https://gitlab.com/Never_Rei/url-shortener',
    version='1.0',
    long_description="A Quick URL Shortener, using Flask and Redis.",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "Bootstrap-Flask==1.5.3",
        "click==8.0.1",
        "Flask==2.0.1",
        "flask-redis==0.4.0",
        "Flask-WTF==0.15.1",
        "itsdangerous==2.0.1",
        "Jinja2==3.0.1",
        "MarkupSafe==2.0.1",
        "redis==3.5.3",
        "waitress==2.0.0",
        "Werkzeug==2.0.1",
        "WTForms==2.3.3"
        ]
)